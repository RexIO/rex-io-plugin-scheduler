# Rex.IO Job Scheduler

Simple Cron-Like Job Scheduler.

This Scheduler is for executing recuring background tasks.

## Configuration:

/etc/rex/io/scheduler.conf

```
# database configuration
<Database>
  class    = mysql
  server   = localhost
  user     = rexio
  password = rexio
  schema   = rexio_server
  port     = 3306
</Database>


<Log4perl>
  config = /etc/rex/io/scheduler/log4perl.conf
</Log4perl>
```

/etc/rex/io/scheduler/log4perl.conf

```
log4perl.rootLogger                    = DEBUG, FileAppndr1

log4perl.appender.FileAppndr1          = Log::Log4perl::Appender::File
log4perl.appender.FileAppndr1.filename = var/log/rex-io/scheduler.log
log4perl.appender.FileAppndr1.layout   = Log::Log4perl::Layout::SimpleLayout
```
