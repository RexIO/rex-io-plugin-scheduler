#
# (c) Jan Gehring <jan.gehring@gmail.com>
#
# vim: set ts=2 sw=2 tw=0:
# vim: set expandtab:

package Rex::IO::Scheduler::Action::EXECUTE;

use strict;
use warnings;

use Moo;
use Carp;

has app => ( is => 'ro' );

sub execute {
  my ( $self, $command ) = @_;

  $self->app->logger->debug("Executing class $command");

  eval "use $command";
  if($@) {
    my $e = $@;
    $self->app->logger->error("Error executing $command.\n\nERROR: $e\n\n");
    confess "Error executing $command.\n\nERROR: $e\n\n";
  }
}

1;
