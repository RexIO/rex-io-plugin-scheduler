#
# (c) Jan Gehring <jan.gehring@gmail.com>
#
# vim: set ts=2 sw=2 tw=0:
# vim: set expandtab:

package Rex::IO::Scheduler;

use strict;
use warnings;

use Moo;
use Schedule::Cron;
use Data::Dumper;
use Carp;
use Try::Tiny;

has config => ( is => 'ro' );
has logger => ( is => 'ro' );
has db     => ( is => 'ro' );
has cron   => ( is => 'rwp' );

sub run {
  my ($self) = @_;

  $self->_set_cron(
    Schedule::Cron->new(
      sub {
        $self->dispatch(@_);
      }
    )
  );

  my $sth = $self->db->prepare(
    "SELECT second, minute, hour, day_of_month, month, day_of_week, command "
      . " FROM scheduler" );
  $sth->execute;

  while ( my $row = $sth->fetchrow_hashref ) {
    my $second       = $row->{second}       || 0;
    my $minute       = $row->{minute}       || 0;
    my $hour         = $row->{hour}         || 0;
    my $day_of_month = $row->{day_of_month} || 0;
    my $month        = $row->{month}        || 0;
    my $day_of_week  = $row->{day_of_week}  || 0;

    my $str = "$minute $hour $day_of_month $month $day_of_week $second";
    $self->logger->debug("Adding $str to crontab.");

    $self->cron->add_entry( $str, EXECUTE => $row->{command} );
  }

  $self->cron->run();
}

sub dispatch {
  my ( $self, $id, $command ) = @_;

  my $klass = "Rex::IO::Scheduler::Action::$id";

  try {
    eval "use $klass";
  }
  catch {
    $self->logger->error("Error loading $klass.\n\nERROR: $_\n\n");
    confess "Error loading $klass.\n\nERROR: $_\n\n";
  };

  my $dispatch_class = $klass->new(app => $self);
  $dispatch_class->execute($command);
}

1;
