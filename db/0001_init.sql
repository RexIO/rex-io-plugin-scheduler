CREATE  TABLE `rexio_server`.`scheduler` (
  `id` INT NOT NULL ,
  `name` VARCHAR(255) NULL ,
  `second` VARCHAR(45) NULL ,
  `minute` VARCHAR(45) NULL ,
  `hour` VARCHAR(45) NULL ,
  `day_of_month` VARCHAR(45) NULL ,
  `month` VARCHAR(45) NULL ,
  `day_of_week` VARCHAR(45) NULL ,
  `command` VARCHAR(45) NULL ,
  `description` TEXT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;
